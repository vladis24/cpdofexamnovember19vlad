package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class MinMaxTest
{
	@Test
	public void testMax() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertEquals(24, minmax.findMax(24, 3));
	}
	
	@Test
	public void testMaxEqual() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertEquals(24, minmax.findMax(24, 24));
	}

	@Test
	public void testMaxNegativeScenario() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertNotEquals(3, minmax.findMax(24, 3));
	}
	
	@Test
	public void testMaxSecond() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertEquals(24, minmax.findMax(3, 24));
	}

}
